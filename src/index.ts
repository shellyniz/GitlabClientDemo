import app from './App'
import { configure, getLogger, Logger } from 'log4js';
const port =  3001;
this.logger = getLogger();
this.logger.level = 'debug';

app.listen(port, (err) => {
  if (err) {
    return console.log(err)
  }
  return this.logger.debug(`server is listening on ${port}`)
})
