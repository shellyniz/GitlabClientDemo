import * as express from 'express'
import * as request from 'request';
import * as ws from 'ws';
import * as bodyparser from 'body-parser';
import { configure, getLogger, Logger } from 'log4js';

class App {
  public express: any;
  public logger: Logger;
  public websocket: ws;
  constructor() {
    this.init();
  }

  init(): void {
    this.createLogger();
    this.express = express();
    this.express.use('../config', express.static('config'));
    this.express.use(bodyparser.json());
    this.mountRoutes();
  }

  // Loggers
  private createLogger(): void {
    this.logger = getLogger();
    this.logger.level = 'debug';
  }


  public onError = () => evt => {
    this.logger.debug('onError() evt=', evt.type);
  }
  public onOpen = () => evt => {
    this.logger.debug('onOpen() status=', evt.type, ', url=', evt.target.url);
  }
  public onClose = () => evt => {
    this.logger.debug('onClose() evt=', evt.type);
  }
  public stateChanged = (proxy) => data => {
 this.logger.debug('Changed');
  };

  // Repository API
  private mountRoutes(): void {
    this.logger.debug('mountRoutes(), create all api of Repository');
    const router = express.Router()
    // router.get('/test', (req, res) => {
    //   this.logger.debug(req);
    //   res.setHeader('Content-Type', 'application/json');
    //   res.json({ 'test': 'test' });
    // });
    router.post('/test', (req, res) => {
      //debugger;
      this.logger.debug((req.body));
      res.setHeader('Content-Type', 'application/json');
      res.json({ 'test': 'test' });
    });

    this.express.use('/', router)
  }

}
export default new App().express
