# Gitlab Client

This is a demo ,using nodejs express, for listening on gitlab system hooks.

## How to use this demo
In order to use this server, you must first add a system hook to the Gitlab server:

(go to admin area->system hooks and add a system hook with your serevr url, port 3001/test, i.e. http://10.3.24.26:3001/test)

Once ther server is up and running, and the system hook is defined, you can either test the hook from the system hooks
page in the admin area of your gitlab server, or craet an actual event (by creating a new user, updating a project, etc.)

Once the serve will get the request from gitlab, it shall print the requst's body to the log.

## Sample Output:
```js
{ event_name: 'repository_update',
  user_id: 8,
  user_name: 'Nezri Shelly',
  user_email: 'shelly.nezri@elbitsystems.com',
  user_avatar: '/uploads/-/system/user/avatar/8/avatar.png',
  project_id: 61,
  project:
   { name: 'GitlabClient',
     description: 'Listen on Gitlab server events',
     web_url: 'http://10.0.213.12/DP24961/GitlabClient',
     avatar_url: null,
     git_ssh_url: 'git@10.0.213.12:DP24961/GitlabClient.git',
     git_http_url: 'http://10.0.213.12/DP24961/GitlabClient.git',
     namespace: 'DP24961',
     visibility_level: 20,
     path_with_namespace: 'DP24961/GitlabClient',
     default_branch: 'master',
     ci_config_path: null,
     homepage: 'http://10.0.213.12/DP24961/GitlabClient',
     url: 'git@10.0.213.12:DP24961/GitlabClient.git',
     ssh_url: 'git@10.0.213.12:DP24961/GitlabClient.git',
     http_url: 'http://10.0.213.12/DP24961/GitlabClient.git' },
  changes:
   [ { before: 'c8155d1c12c81c72fafa99032b25c24edeb43575',
       after: 'cd0a7eb6ff4d3e4f7bef3dff8aaf197fc34de899',
       ref: 'refs/heads/master' } ],
  refs: [ 'refs/heads/master' ] }
  ```